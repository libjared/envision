# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the envision package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: envision\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-18 11:17+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: Do NOT translate or transliterate this text
#: data/org.gabmus.envision.desktop.in.in:4
msgid "@PRETTY_NAME@"
msgstr ""

#: data/org.gabmus.envision.desktop.in.in:5
#: data/org.gabmus.envision.metainfo.xml.in.in:7
#: data/org.gabmus.envision.metainfo.xml.in.in:9
msgid "GUI for Monado"
msgstr ""

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gabmus.envision.desktop.in.in:11
msgid "vr;virtual;reality;monado;"
msgstr ""
